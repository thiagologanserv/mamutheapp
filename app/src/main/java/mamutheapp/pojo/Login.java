package mamutheapp.pojo;

import com.google.gson.annotations.SerializedName;

public class Login {

    /**
     * Aqui vão todos os parametros necessários para que seja realizada
     * a chamada ou envio das informações para a API
     * <p>
     * os "gets and setters" deven estar nessa classe.
     */

    @SerializedName("password")
    String password;

    @SerializedName("username")
    String username;

    @SerializedName("status")
    String status;

    @SerializedName("msg")
    String msg;

    public Login(String username, String password) {
        this.password = password;
        this.username = username;

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
