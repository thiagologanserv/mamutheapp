package mamutheapp.endpoint;

import mamutheapp.pojo.Login;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface EndPoint {

    /**
     * Aqui serão definidos os parametros e as anotações de metodos de solicitação HTTP.
     *
     * @GET
     * @POST
     *
     * Os parametros desse metodo podem ter anotações especiais:
     *
     * @Path
     * Substituição de variável para o ponto de extremidade da API (ou seja, o nome de usuário será trocado {username}no ponto final da URL).
     *
     * @Query
     * Especifica o nome da chave de consulta com o valor do parâmetro anotado.
     *
     * @Body
     * Carga útil para a chamada POST (serializada de um objeto Java para uma cadeia JSON)
     *
     * @Header
     * Especifica o cabeçalho com o valor do parâmetro anotado
     *
     */
    @FormUrlEncoded
    @GET("mobile-auth.php")
    void getStatus(@Field("status") String status,
                   @Field("msg") String msg);

    @Headers("Content-Type: application/json")
    @POST("mobile-auth.php")
    Call<Login> postUser(@Body Login body

//            @Field("username") String username,
//                         @Field("password") String password
    );

}
