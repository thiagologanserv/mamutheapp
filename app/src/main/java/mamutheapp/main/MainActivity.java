package mamutheapp.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.mamuthe.mamutheapp.R;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import mamutheapp.constants.Constants;
import mamutheapp.endpoint.EndPoint;
import mamutheapp.pojo.Login;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private String url;
    private Button btLogar;
    private Context context;
    private Login usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();
        url = Constants.BASE_URL;


        /**
         * Idedntificar itens do Layout
         */
        final EditText login = findViewById(R.id.username);
        final EditText senha = findViewById(R.id.password);
        senha.setTransformationMethod(new PasswordTransformationMethod());
        btLogar = findViewById(R.id.btLogar);



        /**
         * Setar o click do botão de logar para que seja feita a chamada no Retrofit.
         */

        btLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lg = login.getText().toString();
                String pwd = senha.getText().toString();
                usuario = new Login(lg, pwd);

                /**
                 * fazer a chamada de rede da API via Retrofit
                 */
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(String.valueOf(url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                EndPoint servicoApi = retrofit.create(EndPoint.class);

                /**
                 * Fazer o POST de um Login (Exemplo)
                 */

                Call<Login> call = servicoApi.postUser(new Login(usuario.getUsername(), usuario.getPassword()));
                call.enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Call<Login> call, Response<Login> response) {

                        if (!response.isSuccessful()) {
                            //Caso haja erros na comunicação
                            Toast.makeText(getBaseContext(),response.body().getMsg(),Toast.LENGTH_SHORT).show();
                        } else {
                            //Comunicação realizada - exibir mensagem de Login
                            String msg = response.body().getMsg();
                            String status = response.body().getStatus();

                            Toast.makeText(context, "Mensagem de Login - "+ msg + "'\n'Status - "+status, Toast.LENGTH_LONG).show();
                            Log.i("Username", "Username - "+usuario.getUsername());
                            Log.i("Password", "Password - "+usuario.getPassword());

                            /**
                             * Armazenar Coockies para iniciar sessão WebView
                             */
                            CookieManager cookieManager = new CookieManager();
                            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

                            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                            OkHttpClient.Builder oktHttpClient = new OkHttpClient.Builder()
                                    .connectTimeout(60*5, TimeUnit.MINUTES)
                                    .writeTimeout(60*5, TimeUnit.MINUTES)
                                    .readTimeout(60*5, TimeUnit.MINUTES);
                            oktHttpClient.addInterceptor(logging);

                            /**
                             * Iniciar Activity com WebView
                             */
                            Intent intent = new Intent(MainActivity.this, WebViewMamuthe.class);
                            startActivity(intent);
                        }
                    }
                    @Override
                    public void onFailure(Call<Login> call, Throwable t) {
                        Toast toast = Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG);
                        toast.show();

                    }
                });


            }
        });
    }
}